import queue
from dataclasses import dataclass, field
from typing import Any
import time

import numpy as np
from sklearn.metrics import accuracy_score
from skopt.utils import use_named_args
from skopt import gp_minimize

from models.base import BaseModel
from data_utils.embeddings import EmbeddingLoader

def gpopt_search(Model, dataset, iterations):
    @dataclass(order=True)
    class Item:
        score: float
        model: Any = field(compare=False)

    space = Model.get_gp_config_parameters()
    priority_queue = queue.PriorityQueue()
    if Model.require_embeddings():
        embed_loader = EmbeddingLoader()

    t0 = time.time()
    average_length = np.mean(dataset.X_train.text.apply(lambda x: len(x.split())))
    # dirty hardcode
    num_epoches = int(40 * 11000 / len(dataset.X_train) * 21 / average_length)
    num_epoches = min(num_epoches, 40)
    num_epoches = max(num_epoches, 20)

    print("Number of epoches to train =", num_epoches)

    @use_named_args(space)
    def objective(**config):
        if Model.require_embeddings():
            config['embeddings_loader'] = embed_loader
        config['num_classes'] = len(set(dataset.y_train.label))
        config['verbose'] = False
        config['average_length'] = average_length
        config['num_epoches'] = num_epoches

        model = Model(config)
        score = model.train(dataset)
        priority_queue.put(Item(score, model))

        return score

    res_gp = gp_minimize(objective, space, n_calls=iterations, random_state=0)

    print(res_gp['x'])
    print("Optimization time =", time.time()-t0)

    if Model.stack_models():
        models = []
        for i in range(5):
            item = priority_queue.get()
            models.append(item.model)
            print("Validation score with model {} = {}".format(i, item.score))

        print()
        predictions_val = np.zeros((len(dataset.X_val), len(set(dataset.y_train.label))))
        predictions_test = np.zeros((len(dataset.X_test), len(set(dataset.y_train.label))))

        for m in models:
            preds = m.predict(dataset.X_val.text)
            predictions_val += np.array(preds)

            preds = m.predict(dataset.X_test.text)
            y_pred_labels = np.argmax(preds, axis=1)
            accuracy_test_model = accuracy_score(dataset.y_test['label'].astype(int), y_pred_labels)
            print('Test score on model =', accuracy_test_model)
            predictions_test += np.array(preds)

        predictions_val /= 5
        predictions_test /= 5

        y_pred_val = np.argmax(predictions_val, axis=1)
        y_pred_test = np.argmax(predictions_test, axis=1)

        accuracy_val = accuracy_score(dataset.y_val['label'].astype(int), y_pred_val)
        print('Val score on dataset =', accuracy_val)
        accuracy_test_model = accuracy_score(dataset.y_test['label'].astype(int), y_pred_test)
        print('Test score on dataset =', accuracy_test_model)

    else:
        item = priority_queue.get()
        best_score_val, best_model = item.score, item.model
        print("Best score on validation =", best_score_val)
        best_model.validate_on_test(dataset)
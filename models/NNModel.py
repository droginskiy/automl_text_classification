from collections import Counter

import numpy as np
from keras.preprocessing.sequence import pad_sequences
from nltk.tokenize import RegexpTokenizer

from .base import BaseModel

class NNModel(BaseModel):
    def get_text_processor(self, vecs, seq_length, words_limit, tokenizer):
        class PretrainedEmbeddingsProcessor:
            def __init__(self, vecs, seq_length, words_limit, tokenizer):
                # word index 0 is reserved for padding, word index 1 means OOV word
                self.word_to_index = dict()
                self.counter = Counter()
                self.tokenizer = tokenizer
                self.sequence_length = seq_length
                self.words_limit = words_limit
                self.embeddings = np.random.randn(words_limit, 300) - 0.5
                self.pretrained = vecs

            def _tokenize(self, X):
                X = [' '.join(self.tokenizer.tokenize(x)) for x in X]

                return X

            def fit(self, X, *args):
                X = self._tokenize(X)
                for x in X:
                    self.counter.update(x.split(" "))

                allvecs = []
                for i, (w, cnt) in enumerate(list(self.counter.most_common(self.words_limit - 2))):
                    self.word_to_index[w] = i + 2
                    if w in self.pretrained:
                        self.embeddings[i + 2, :] = self.pretrained[w]
                        allvecs.extend(list(self.pretrained[w]))

                # rescaling according to https://www.aclweb.org/anthology/D14-1181.pdf
                a = np.sqrt(12)*np.std(allvecs)
                for i, (w, cnt) in enumerate(list(self.counter.most_common(self.words_limit - 2))):
                    self.word_to_index[w] = i + 2
                    if w not in self.pretrained:
                        self.embeddings[i + 2, :] *= a

            def transform(self, X, *args):
                X = self._tokenize(X)
                seq = [[self.word_to_index.get(w, 1) for w in x.split(" ")] for x in X]
                seq = pad_sequences(seq, maxlen=self.sequence_length, padding='post', truncating='post')
                return seq

            def fit_transform(self, X, *args):
                self.fit(X)
                return self.transform(X)

        proc = PretrainedEmbeddingsProcessor(vecs, seq_length, words_limit, tokenizer)
        return proc
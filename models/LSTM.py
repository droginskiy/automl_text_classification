import random
from collections import Counter

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from keras.preprocessing.sequence import pad_sequences
from nltk.tokenize import RegexpTokenizer
from sklearn.metrics import accuracy_score
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from hyperopt import hp
from skopt.space import Real, Integer, Categorical


from .NNModel import NNModel


class LSTMPred(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, embeddings, num_classes, seq_length, dropout_rate=0.5,
                 num_layers=1, device='cuda', use_bidirectional=False, freeze_embeddings=True):
        super(LSTMPred, self).__init__()
        self.num_layers = num_layers
        self.device = device
        self.hidden_dim = hidden_dim
        self.bidir_mult = 2 if use_bidirectional else 1
        self.seq_length = seq_length
        weight = torch.FloatTensor(embeddings)

        self.word_embeddings = nn.Embedding.from_pretrained(weight, freeze_embeddings)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=num_layers, bidirectional=use_bidirectional)
        self.hidden2pred = nn.Linear(num_layers * self.bidir_mult * self.hidden_dim, num_classes)
        self.hidden = self.init_hidden(1)
        self.dropout = nn.Dropout(dropout_rate)

    def init_hidden(self, batch_size):
        return (torch.zeros(self.bidir_mult * self.num_layers, batch_size, self.hidden_dim, device=self.device),
                torch.zeros(self.bidir_mult * self.num_layers, batch_size, self.hidden_dim, device=self.device))

    def forward(self, X_batch, lengths):
        embeds = self.word_embeddings(X_batch)
        packed_seq = pack_padded_sequence(embeds, lengths)
        self.hidden = self.init_hidden(len(lengths))
        lstm_out, self.hidden = self.lstm(packed_seq, self.hidden)
        h_n, c_n = self.hidden
        preds = self.hidden2pred(self.dropout(torch.transpose(h_n, 0, 1).contiguous().view(len(lengths), -1)))
        return preds

class LSTMModel(NNModel):
    def __init__(self, config):
        self.vocabulary_size = int(config['vocabulary_size'])
        self.seq_length = int(config['average_length'] * config['ratio_seq_length'])
        self.hidden_dim = int(config['hidden_dim'])
        self.lr = config['lr']
        self.num_classes = int(config['num_classes'])
        self.batch_size = 2 ** int(config['batch_size'])
        self.verbose = config['verbose']
        self.dropout_rate = config['dropout_rate']
        self.num_layers = int(config['num_layers'])
        self.epoches = config['num_epoches']
        # self.epoches = 100
        self.use_bidirectional = config['use_bidirectional']
        self.l2reg = config['l2reg']
        self.vecs = config['embeddings_loader'].get(config['embedding_type'])
        self.freeze_embeddings = config['freeze_embeddings']
        self.early_stopping = self.is_optimizable()
        self.tokenizer = RegexpTokenizer(r'([a-zA-Z0-9]+|[\.\,\?])') if config['embedding_type'] == 'fasttext'\
                                                             else RegexpTokenizer(r'[a-zA-Z0-9]+')

        self.proc = self.get_text_processor(self.vecs, self.seq_length, self.vocabulary_size, self.tokenizer)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.model = LSTMPred(300, self.hidden_dim, self.proc.embeddings, self.num_classes, self.seq_length,
                              self.dropout_rate, self.num_layers, self.device, self.use_bidirectional,
                              freeze_embeddings=self.freeze_embeddings).to(self.device)

    def get_l2_reg_loss(self):
        l = torch.tensor(self.l2reg).to(self.device)
        l2_reg = torch.tensor(0.).to(self.device)
        for param in self.model.hidden2pred.parameters():
            l2_reg += l * torch.norm(param).pow(2)

        return l2_reg

    def train(self, dataset):
        self.proc.fit(dataset.X_train.text.tolist())
        self.loss_function = nn.CrossEntropyLoss().to(self.device)
        optimizer = optim.Adam(self.model.parameters())
        X = self.proc.transform(dataset.X_train.text.tolist())
        y = dataset.y_train.label.tolist()
        X_val = self.proc.transform(dataset.X_val.text.tolist())
        y_val = np.array(dataset.y_val.label).reshape((-1,))

        inds = list(range(X.shape[0]))
        y = np.array(y).reshape((-1,))

        best_accuracy = 0

        for epoch in range(self.epoches):
            self.model.train()
            random.seed(epoch)
            random.shuffle(inds)
            for i in range(0, len(inds), self.batch_size):
                inds_batch = inds[i:i + self.batch_size]
                X_batch = X[inds_batch, :]
                y_batch = y[inds_batch]

                self.model.zero_grad()

                lengths = np.sum(X_batch != 0, axis=1)
                lengths = np.clip(lengths, 1, self.seq_length)
                args = np.argsort(-lengths)
                predictions = self.model.forward(torch.LongTensor(X_batch[args, :].T).to(self.device), lengths[args])

                output = self.loss_function(predictions, torch.LongTensor(y_batch[args]).to(self.device))
                reg_loss = self.get_l2_reg_loss()
                loss = output + reg_loss
                loss.backward()
                optimizer.step()

            val_loss, val_acc = self.evaluate(X_val, y_val)

            if best_accuracy < val_acc:
                best_accuracy = val_acc
                torch.save(self.model, 'data/best_model.pt')

            if best_accuracy < 1/self.num_classes*1.1:
                break
            if self.verbose:
                train_loss, train_acc = self.evaluate(X[inds[:X_val.shape[0]], :], y[inds[:X_val.shape[0]]])
                print('Epoch {}. Train loss = {} Train acc = {} Val loss = {} Val acc = {}'
                      .format(epoch, train_loss, train_acc, val_loss, val_acc))
        self.model = torch.load('data/best_model.pt')
        return -best_accuracy

    def evaluate(self, X, y):
        self.model.eval()
        y = np.array(y).reshape((-1))
        predicted_answers = []
        losses = []
        shuffled_y = []
        for i in range(0, len(X), self.batch_size):
            X_batch = X[i:i + self.batch_size, :]
            y_batch = y[i:i + self.batch_size]
            lengths = np.sum(X_batch != 0, axis=1)
            lengths = np.clip(lengths, 1, self.seq_length)
            args = np.argsort(-lengths)
            shuffled_y.extend(list(y_batch[args]))
            predictions = self.model.forward(torch.LongTensor(X_batch[args, :].T).to(self.device),
                                             lengths[args])
            output = self.loss_function(predictions, torch.LongTensor(y_batch[args]).to(self.device))
            reg_loss = self.get_l2_reg_loss()
            loss = output + reg_loss

            losses.append(loss.item())
            predictions = predictions.cpu().detach().numpy()
            y_preds = np.argmax(predictions, axis=1)

            predicted_answers.extend(list(y_preds))

        y_preds = predicted_answers

        return np.mean(losses), accuracy_score(shuffled_y, y_preds)

    def predict(self, X):
        self.model.eval()
        X = self.proc.transform(X)
        predicted_answers = []
        for i in range(0, len(X), self.batch_size):
            X_batch = X[i:i + self.batch_size, :]
            lengths = np.sum(X_batch != 0, axis=1)
            lengths = np.clip(lengths, 1, self.seq_length)
            args = np.argsort(-lengths)
            reverse_args = np.argsort(args)
            predictions = nn.functional.softmax(self.model.forward(torch.LongTensor(X_batch[args, :].T).to(self.device),
                                             lengths[args]), dim=1)

            predictions = predictions.cpu().detach().numpy()
            predicted_answers.append(predictions[reverse_args, :])

        return  np.concatenate(predicted_answers, axis=0)


    @staticmethod
    def get_noopt_config_parameters():
        config = {'hidden_dim': 128, 'vocabulary_size': 50000, 'ratio_seq_length': 1, 'lr': 0.01, 'num_classes': 20,
                  'batch_size': 6, 'verbose': True, 'dropout_rate': 0.5, 'num_layers': 1, 'epoches': 50,
                  'use_bidirectional': True, 'l2reg': 0.0, 'embedding_type': 'fasttext'}

        return config

    @staticmethod
    def get_config_parameters():
        config = {'hidden_dim': hp.quniform('hidden_dim', 32, 256, 1),
                  'vocabulary_size': hp.quniform('vocabulary_size', 10000, 100000, 1),
                  'ratio_seq_length': hp.uniform('ratio_seq_length', 0.5, 2),
                  'lr': hp.loguniform('lr', np.log(0.0001), np.log(0.01)),
                  'batch_size': hp.quniform('batch_size', 5.5, 8.5, 1),
                  'dropout_rate': hp.uniform('dropout_rate', 0.01, 0.8), 'num_layers': hp.choice('num_layers', [1, 2]),
                  'use_bidirectional': hp.choice('use_bidirectional', [True, False]),
                  'l2reg': hp.loguniform('l2reg', np.log(0.000001), np.log(0.5)),
                  'freeze_embeddings': hp.choice('freeze_embeddings', [True, False]),
                  'embedding_type': hp.choice('embedding_type', ['fasttext', 'glove', 'w2v'])}

        return config

    @staticmethod
    def get_gp_config_parameters():
        space = [Integer(32, 256, name='hidden_dim'),
                 Integer(10000, 100000, name='vocabulary_size'),
                 Real(0.5, 2, name='ratio_seq_length'),
                 Real(10 ** -4, 0.01, "log-uniform", name='lr'),
                 Integer(5, 8, name='batch_size'),
                 Real(0.01, 0.8, name='dropout_rate'),
                 Integer(1, 2, name='num_layers'),
                 Categorical([True, False], name='use_bidirectional'),
                 Categorical([True, False], name='freeze_embeddings'),
                 Real(0.000001, 0.5, "log-uniform", name='l2reg'),
                 Categorical(['fasttext', 'glove', 'w2v'], name='embedding_type')]

        return space

    @staticmethod
    def is_optimizable():
        return True

    @staticmethod
    def require_embeddings():
        return True

    @staticmethod
    def stack_models():
        return True
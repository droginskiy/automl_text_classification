from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import numpy as np
from hyperopt import hp
from skopt.space import Real, Integer, Categorical

from .base import BaseModel


class LogRegModel(BaseModel):
    def __init__(self, config):
        self.max_ngram = config['max_ngram']
        self.min_df = config['min_df']
        self.C = config['C']
        self.vec = TfidfVectorizer(ngram_range=(1, int(self.max_ngram)), min_df=int(self.min_df))
        self.lr = LogisticRegression(C=self.C)

    @staticmethod
    def get_config_parameters():
        # quniform produces random integer with formula round(uniform(low, high) / q) * q
        space = {'max_ngram': hp.quniform('max_ngram', 0.5, 3.5, 1), 'min_df': hp.quniform('min_df', 0.5, 20.5, 1),
                 'C': hp.loguniform('C', -7, 3)}

        return space

    @staticmethod
    def get_gp_config_parameters():
        space = [Integer(1, 3, name='max_ngram'),
                 Integer(1, 20, name='min_df'),
                 Real(np.exp(-7), np.exp(3), "log-uniform", name='C')]

        return space

    def train(self, dataset):
        X = self.vec.fit_transform(dataset.X_train['text'])
        self.lr.fit(X, dataset.y_train['label'])

        X_val = self.vec.transform(dataset.X_val['text'])
        y_pred = self.lr.predict(X_val)

        accuracy = -sum(y_pred == dataset.y_val['label'].astype(int))/len(y_pred)
        print(accuracy)

        return accuracy

    def predict(self, X):
        X_transformed = self.vec.transform(X)
        return self.lr.predict(X_transformed)

    @staticmethod
    def is_optimizable():
        return True

    def validate_on_test(self, dataset):
        X_test = self.vec.transform(dataset.X_test['text'])
        y_pred = self.lr.predict(X_test)

        accuracy = accuracy_score(dataset.y_test['label'].astype(int), y_pred)
        print('Test score on dataset =', accuracy)

        return accuracy
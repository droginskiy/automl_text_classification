class BaseModel:
    def __init__(self, config):
        raise NotImplementedError

    def train(self, dataset):
        raise NotImplementedError

    def predict(self, X):
        raise NotImplementedError

    @staticmethod
    def get_config_parameters():
        raise NotImplementedError

    @staticmethod
    def get_gp_config_parameters():
        raise NotImplementedError

    @staticmethod
    def is_optimizable():
        raise NotImplementedError

    @staticmethod
    def require_embeddings():
        return False

    @staticmethod
    def stack_models():
        return False

    def validate_on_test(self, dataset):
        raise NotImplementedError
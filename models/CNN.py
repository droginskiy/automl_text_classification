import random

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from hyperopt import hp
from nltk.tokenize import RegexpTokenizer
from sklearn.metrics import accuracy_score
from skopt.space import Real, Integer, Categorical

from .NNModel import NNModel


class CNNPred(nn.Module):

    def __init__(self, embedding_dim, embeddings, num_classes, seq_length, dropout_rate=0.5, device='cuda',
                 conv_output=64, freeze_embeddings=True):
        super(CNNPred, self).__init__()
        self.device = device
        self.seq_length = seq_length
        self.conv_output = conv_output
        self.relu3 = nn.ReLU()
        self.relu4 = nn.ReLU()
        self.relu5 = nn.ReLU()
        weight = torch.FloatTensor(embeddings)

        self.word_embeddings = nn.Embedding.from_pretrained(weight, freeze_embeddings)

        self.conv3 = nn.Conv1d(embedding_dim, self.conv_output, 3)
        self.conv4 = nn.Conv1d(embedding_dim, self.conv_output, 4)
        self.conv5 = nn.Conv1d(embedding_dim, self.conv_output, 5)

        self.max_pool3 = nn.MaxPool1d(seq_length - 3 + 1)
        self.max_pool4 = nn.MaxPool1d(seq_length - 4 + 1)
        self.max_pool5 = nn.MaxPool1d(seq_length - 5 + 1)
        self.pred_dropout = nn.Dropout(dropout_rate)
        self.hidden2pred = nn.Linear(3 * conv_output, num_classes)

    def forward(self, X_batch):
        embeds = self.word_embeddings(X_batch)

        convoluted3 = self.conv3(torch.transpose(embeds, 1, 2))
        convoluted4 = self.conv4(torch.transpose(embeds, 1, 2))
        convoluted5 = self.conv5(torch.transpose(embeds, 1, 2))

        pooled3 = self.max_pool3(self.relu3(convoluted3))
        pooled4 = self.max_pool4(self.relu4(convoluted4))
        pooled5 = self.max_pool5(self.relu5(convoluted5))

        concatenated = torch.cat((pooled3, pooled4, pooled5), 1)
        preds = self.hidden2pred(self.pred_dropout(concatenated.view(X_batch.shape[0], -1)))
        return preds


class CNNModel(NNModel):
    def __init__(self, config):
        self.vocabulary_size = int(config['vocabulary_size'])
        self.seq_length = int(config['average_length'] * config['ratio_seq_length'])
        self.lr = config['lr']
        self.num_classes = int(config['num_classes'])
        self.batch_size = 2 ** int(config['batch_size'])
        self.verbose = config['verbose']
        self.dropout_rate = config['dropout_rate']
        self.epoches = config['num_epoches']
        self.l2reg = config['l2reg']
        self.conv_output = int(config['conv_output'])
        self.vecs = config['embeddings_loader'].get(config['embedding_type'])
        self.freeze_embeddings = config['freeze_embeddings']
        self.early_stopping = self.is_optimizable()
        # self.hidden_dense_size = int(config['hidden_dense_size'])
        self.tokenizer = RegexpTokenizer(r'([a-zA-Z0-9]+|[\.\,\?])') if config['embedding_type'] == 'fasttext'\
                                                             else RegexpTokenizer(r'[a-zA-Z0-9]+')

        self.proc = self.get_text_processor(self.vecs, self.seq_length, self.vocabulary_size, self.tokenizer)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.model = CNNPred(300, self.proc.embeddings, self.num_classes, self.seq_length,
                             self.dropout_rate, self.device,
                             self.conv_output, self.freeze_embeddings).to(self.device)

    def get_l2_reg_loss(self):
        l = torch.tensor(self.l2reg).to(self.device)
        l2_reg = torch.tensor(0.).to(self.device)
        for param in self.model.hidden2pred.parameters():
            l2_reg += l * torch.norm(param).pow(2)

        return l2_reg

    def train(self, dataset):
        self.proc.fit(dataset.X_train.text.tolist())
        self.loss_function = nn.CrossEntropyLoss().to(self.device)
        optimizer = optim.Adam(self.model.parameters(),lr=self.lr)
        X = self.proc.transform(dataset.X_train.text.tolist())
        y = dataset.y_train.label.tolist()
        X_val = self.proc.transform(dataset.X_val.text.tolist())
        y_val = np.array(dataset.y_val.label).reshape((-1,))

        inds = list(range(X.shape[0]))
        y = np.array(y).reshape((-1,))

        # if self.early_stopping:
        best_accuracy = 0

        for epoch in range(self.epoches):
            self.model.train()
            random.seed(epoch)
            random.shuffle(inds)
            for i in range(0, len(inds), self.batch_size):
                inds_batch = inds[i:i + self.batch_size]
                X_batch = X[inds_batch, :]
                y_batch = y[inds_batch]

                self.model.zero_grad()

                predictions = self.model.forward(torch.LongTensor(X_batch).to(self.device))

                output = self.loss_function(predictions, torch.LongTensor(y_batch).to(self.device))
                loss = output + self.get_l2_reg_loss()
                loss.backward()
                optimizer.step()

            val_loss, val_acc = self.evaluate(X_val, y_val)
            if best_accuracy < val_acc:
                best_accuracy = val_acc
                torch.save(self.model, 'data/best_model.pt')
            if best_accuracy < 1/self.num_classes*1.1:
                break
            if self.verbose:
                train_loss, train_acc = self.evaluate(X[inds[:X_val.shape[0]], :], y[inds[:X_val.shape[0]]])
                print('Epoch {}. Train loss = {} Train acc = {} Val loss = {} Val acc = {}'
                      .format(epoch, train_loss, train_acc, val_loss, val_acc))
        self.model = torch.load('data/best_model.pt')
        return -best_accuracy

    def evaluate(self, X, y):
        self.model.eval()
        y = np.array(y).reshape((-1))
        predicted_answers = []
        losses = []
        for i in range(0, len(X), self.batch_size):
            X_batch = X[i:i + self.batch_size, :]
            y_batch = y[i:i + self.batch_size]

            predictions = self.model.forward(torch.LongTensor(X_batch).to(self.device))
            output = self.loss_function(predictions, torch.LongTensor(y_batch).to(self.device))
            loss = output + self.get_l2_reg_loss()

            losses.append(loss.item())
            predictions = predictions.cpu().detach().numpy()
            y_preds = np.argmax(predictions, axis=1)

            predicted_answers.extend(list(y_preds))

        return np.mean(losses), accuracy_score(y, predicted_answers)

    def predict(self, X):
        self.model.eval()
        X = self.proc.transform(X)
        predicted_answers = []
        for i in range(0, len(X), self.batch_size):
            X_batch = X[i:i + self.batch_size, :]

            predictions = nn.functional.softmax(self.model.forward(torch.LongTensor(X_batch).to(self.device)), dim=1)

            predictions = predictions.cpu().detach().numpy()
            predicted_answers.append(predictions)

        return  np.concatenate(predicted_answers, axis=0)

    @staticmethod
    def get_noopt_config_parameters():
        config = {'vocabulary_size': 50000, 'ratio_seq_length': 1, 'lr': 0.01, 'num_classes': 20,
                  'batch_size': 6, 'verbose': True, 'dropout_rate': 0.5, 'epoches': 50,
                  'l2reg': 0.03, 'conv_output': 100, 'embedding_type': 'fasttext'}

        return config

    @staticmethod
    def get_config_parameters():
        config = {'vocabulary_size': hp.quniform('vocabulary_size', 10000, 100000, 1),
                  'ratio_seq_length': hp.uniform('ratio_seq_length', 0.5, 2),
                  'lr': hp.loguniform('lr', np.log(0.0001), np.log(0.05)),
                  'batch_size': hp.quniform('batch_size', 5.5, 8.5, 1),
                  'dropout_rate': hp.uniform('dropout_rate', 0.01, 0.8),
                  'freeze_embeddings': hp.choice('freeze_embeddings', [True, False]),
                  'l2reg': hp.loguniform('l2reg', np.log(0.000001), np.log(0.5)),
                  'conv_output': hp.quniform('conv_output', 32, 256, 1),
                  'embedding_type': hp.choice('embedding_type', ['fasttext', 'glove', 'w2v'])}

        return config

    @staticmethod
    def get_gp_config_parameters():
        space = [Integer(10000, 100000, name='vocabulary_size'),
                 Real(0.5, 2, name='ratio_seq_length'),
                 Real(10 ** -4, 0.05, "log-uniform", name='lr'),
                 Integer(5, 8, name='batch_size'),
                 Real(0.01, 0.8, name='dropout_rate'),
                 Categorical([True, False], name='freeze_embeddings'),
                 Real(0.000001, 0.5, "log-uniform", name='l2reg'),
                 Integer(32, 256, name='conv_output'),
                 Categorical(['fasttext', 'glove', 'w2v'], name='embedding_type')]

        return space

    @staticmethod
    def is_optimizable():
        return True

    @staticmethod
    def require_embeddings():
        return True

    @staticmethod
    def stack_models():
        return True
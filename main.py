from data_utils.load_datasets import load_dataset
from hpsearch.hyperopt_search import hyperopt_search
from hpsearch.gp_opt import gpopt_search
from models.LogReg import LogRegModel
from models.LSTM import LSTMModel
from models.CNN import CNNModel
import argparse


if __name__ == '__main__':
    AVAILABLE_MODELS = {
        'logreg' : LogRegModel,
        'lstm': LSTMModel,
        'cnn': CNNModel
    }
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", default='imdb')
    parser.add_argument("--iterations", type=int, default=10)
    parser.add_argument("--model", default='logreg')
    parser.add_argument("--load_data", action='store_true')
    parser.add_argument("--optimizer", default='hyperopt')

    args = parser.parse_args()

    if args.load_data:
        ds = load_dataset(args.dataset)
    else:
        dataset = load_dataset(args.dataset)
        model = AVAILABLE_MODELS[args.model]
        if model.is_optimizable():
            if args.optimizer == 'hyperopt':
                hyperopt_search(AVAILABLE_MODELS[args.model], dataset, args.iterations)
            elif args.optimizer == 'gp':
                gpopt_search(AVAILABLE_MODELS[args.model], dataset, args.iterations)
        else:        
            config = model.get_config_parameters()
            model_object = model(config)
            print(model_object.train(dataset))

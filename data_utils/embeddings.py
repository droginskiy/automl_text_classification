import os
from gensim.models.keyedvectors import Word2VecKeyedVectors

class EmbeddingLoader:
    def __init__(self):
        self.embeddings = {}
        if os.path.exists('data/wiki-news-300d-1M.vec'):
            self.embeddings['fasttext'] = Word2VecKeyedVectors.load_word2vec_format('data/wiki-news-300d-1M.vec')
        else:
            if not os.path.exists('tmp'):
                os.mkdir('tmp')
            cmd = """ 
            wget -O tmp/wiki-news-300d-1M.vec.zip https://dl.fbaipublicfiles.com/fasttext/vectors-english/wiki-news-300d-1M.vec.zip && 
            unzip tmp/wiki-news-300d-1M.vec.zip -d data
            """
            os.system(cmd)
            self.embeddings['fasttext'] = Word2VecKeyedVectors.load_word2vec_format('data/wiki-news-300d-1M.vec')

        if os.path.exists('data/glove.6B.300d.w2vformat.txt'):
            self.embeddings['glove'] = Word2VecKeyedVectors.load_word2vec_format('data/glove.6B.300d.w2vformat.txt')
        else:
            if not os.path.exists('tmp'):
                os.mkdir('tmp')
            cmd = """
            wget -O tmp/glove.6B.zip https://nlp.stanford.edu/data/glove.6B.zip && unzip tmp/glove.6B.zip -d tmp && 
            python -m gensim.scripts.glove2word2vec --input  tmp/glove.6B.300d.txt --output data/glove.6B.300d.w2vformat.txt"""

            os.system(cmd)
            self.embeddings['glove'] = Word2VecKeyedVectors.load_word2vec_format('data/glove.6B.300d.w2vformat.txt')

        if os.path.exists('data/GoogleNews-vectors-negative300.bin'):
            self.embeddings['w2v'] = Word2VecKeyedVectors.load_word2vec_format(
                'data/GoogleNews-vectors-negative300.bin', binary=True)
        else:
            if not os.path.exists('tmp'):
                os.mkdir('tmp')
            cmd = """
            wget -O data/GoogleNews-vectors-negative300.bin.gz https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz &&
            gunzip data/GoogleNews-vectors-negative300.bin.gz
            """
            os.system(cmd)

            self.embeddings['w2v'] = Word2VecKeyedVectors.load_word2vec_format(
                'data/GoogleNews-vectors-negative300.bin', binary=True)

        if os.path.exists('tmp'):
            cmd = "rm -r tmp"
            os.system(cmd)

    def get(self, embedding_type):
        return self.embeddings[embedding_type]
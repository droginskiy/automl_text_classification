import os

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


class Dataset:
    def __init__(self, X_train, y_train, X_val, y_val, X_test, y_test):
        self.X_train = X_train
        self.y_train = y_train

        self.X_val = X_val
        self.y_val = y_val

        self.X_test = X_test
        self.y_test = y_test


def report_dataset_stats(dataset):
    print("X_train size =", len(dataset.X_train))
    print("X_val size =", len(dataset.X_val))
    print("X_test size =", len(dataset.X_test))
    print("Average length of sentence in X_train =", np.mean(dataset.X_train.text.apply(lambda x: len(x.split()))))
    print("Number of classes =", len(set(dataset.y_train.label)))
    print()


def load_dataset(name: str) -> Dataset:
    AVAILABLE_DATASETS = {
        'imdb': load_imdb,
        '20news': load_20news,
        'elec': load_elec,
        'movie_reviews': load_movie_reviews,
        'trec': load_trec,
        'subj': load_subj
    }
    if os.path.exists('data/' + name):
        print('Loading dataset from folder')
        dataset = load_from_folder('data/' + name)


    elif name not in AVAILABLE_DATASETS.keys():
        raise KeyError("No such dataset name")
    else:
        print('Downloading dataset', name)
        dataset = AVAILABLE_DATASETS[name]()
    create_google_automl_format(name, dataset)
    report_dataset_stats(dataset)
    return dataset


def load_from_folder(folder: str) -> Dataset:
    X_train = pd.read_csv(folder + '/train/X.csv')
    y_train = pd.read_csv(folder + '/train/y.csv')

    X_val = pd.read_csv(folder + '/val/X.csv')
    y_val = pd.read_csv(folder + '/val/y.csv')

    X_test = pd.read_csv(folder + '/test/X.csv')
    y_test = pd.read_csv(folder + '/test/y.csv')

    return Dataset(X_train, y_train, X_val, y_val, X_test, y_test)


def split_and_save(X_train, y_train, name, test_size=0.2):
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=test_size, random_state=42)

    X_train = pd.DataFrame({'text': X_train})
    y_train = pd.DataFrame({'label': y_train})

    X_val = pd.DataFrame({'text': X_val})
    y_val = pd.DataFrame({'label': y_val})

    os.makedirs('data/{}'.format(name))
    os.mkdir('data/{}/train'.format(name))
    os.mkdir('data/{}/val'.format(name))
    os.mkdir('data/{}/test'.format(name))
    X_train.to_csv('data/{}/train/X.csv'.format(name), index=False)
    y_train.to_csv('data/{}/train/y.csv'.format(name), index=False)

    X_val.to_csv('data/{}/val/X.csv'.format(name), index=False)
    y_val.to_csv('data/{}/val/y.csv'.format(name), index=False)


def save_test(X_test, y_test, name):

    X_test = pd.DataFrame({'text': X_test})
    y_test = pd.DataFrame({'label': y_test})

    X_test.to_csv('data/{}/test/X.csv'.format(name), index=False)
    y_test.to_csv('data/{}/test/y.csv'.format(name), index=False)


def load_imdb() -> Dataset:
    cmd = """
    mkdir tmp && 
    wget -O tmp/aclImdb_v1.tar.gz https://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz &&
    tar -xf tmp/aclImdb_v1.tar.gz -C tmp
    """
    os.system(cmd)
    X_train = []
    y_train = []

    for el in os.listdir('tmp/aclImdb/train/pos'):
        with open('tmp/aclImdb/train/pos/' + el, 'r') as in_f:
            X_train.append(in_f.read())
            y_train.append(1)
    for el in os.listdir('tmp/aclImdb/train/neg'):
        with open('tmp/aclImdb/train/neg/' + el, 'r') as in_f:
            X_train.append(in_f.read())
            y_train.append(0)

    split_and_save(X_train, y_train, 'imdb')

    X_test = []
    y_test = []

    for el in os.listdir('tmp/aclImdb/test/pos'):
        with open('tmp/aclImdb/test/pos/' + el, 'r') as in_f:
            X_test.append(in_f.read())
            y_test.append(1)
    for el in os.listdir('tmp/aclImdb/test/neg'):
        with open('tmp/aclImdb/test/neg/' + el, 'r') as in_f:
            X_test.append(in_f.read())
            y_test.append(0)

    save_test(X_test, y_test, 'imdb')

    cmd = "rm -r tmp"
    os.system(cmd)

    return load_from_folder('data/imdb')


def load_elec() -> Dataset:
    cmd = """
    mkdir tmp && 
    wget -O tmp/elec2.tar.gz http://riejohnson.com/software/elec2.tar.gz &&
    tar -xf tmp/elec2.tar.gz -C tmp
    """
    os.system(cmd)
    X_train = []
    y_train = []
    convert_dict = {'1': 0, '2': 1}

    with open('tmp/elec/elec-25k-train.txt', 'r') as in_f:
        for el in in_f:
            X_train.append(el[:-1])

    with open('tmp/elec/elec-25k-train.cat', 'r') as in_f:
        for el in in_f:
            y_train.append(convert_dict[el[:-1]])

    split_and_save(X_train, y_train, 'elec')

    X_test = []
    y_test = []

    with open('tmp/elec/elec-test.txt', 'r') as in_f:
        for el in in_f:
            X_test.append(el[:-1])

    with open('tmp/elec/elec-test.cat', 'r') as in_f:
        for el in in_f:
            y_test.append(convert_dict[el[:-1]])

    save_test(X_test, y_test, 'elec')

    cmd = "rm -r tmp"
    os.system(cmd)

    return load_from_folder('data/elec')


def load_20news():
    from sklearn.datasets import fetch_20newsgroups
    newsgroups_train = fetch_20newsgroups(subset='train')

    data = newsgroups_train['data']
    target = newsgroups_train['target']
    split_and_save(data, target, '20news')

    newsgroups_test = fetch_20newsgroups(subset='test')
    data = newsgroups_test['data']
    target = newsgroups_test['target']

    save_test(data, target, '20news')

    return load_from_folder('data/20news')

def load_movie_reviews():
    cmd = """
    mkdir tmp && 
    wget -O tmp/rt-polaritydata.tar.gz http://www.cs.cornell.edu/people/pabo/movie-review-data/rt-polaritydata.tar.gz &&
    tar -xf tmp/rt-polaritydata.tar.gz -C tmp
    """
    os.system(cmd)
    X_train = []
    y_train = []
    with open('tmp/rt-polaritydata/rt-polarity.pos', 'r', errors='replace') as in_f:
        for el in in_f:
            X_train.append(el)
            y_train.append(1)
    with open('tmp/rt-polaritydata/rt-polarity.neg', 'r', errors='replace') as in_f:
        for el in in_f:
            X_train.append(el)
            y_train.append(0)

    X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.1, random_state=42)

    split_and_save(X_train, y_train, 'movie_reviews', 1/9)

    save_test(X_test, y_test, 'movie_reviews')

    cmd = "rm -r tmp"
    os.system(cmd)

    return load_from_folder('data/movie_reviews')

def load_trec():
    cmd = """
    mkdir tmp && 
    wget -O tmp/train_5500.label http://cogcomp.org/Data/QA/QC/train_5500.label &&
    wget -O tmp/TREC_10.label http://cogcomp.org/Data/QA/QC/TREC_10.label
    """
    os.system(cmd)
    X_train = []
    y_train = []
    with open('tmp/train_5500.label', 'r', errors='replace') as in_f:
        for el in in_f:
            X_train.append(' '.join(el.split(" ")[1:]))
            y_train.append(el.split(':')[0])
    unique_labels = set(y_train)
    labels_to_ind = {l: i for i, l in enumerate(unique_labels)}
    y_train = [labels_to_ind[label] for label in y_train]
    split_and_save(X_train, y_train, 'trec')

    X_test = []
    y_test = []

    with open('tmp/TREC_10.label', 'r', errors='replace') as in_f:
        for el in in_f:
            X_test.append(' '.join(el.split(" ")[1:]))
            y_test.append(labels_to_ind[el.split(':')[0]])

    save_test(X_test, y_test, 'trec')

    cmd = "rm -r tmp"
    os.system(cmd)

    return load_from_folder('data/trec')

def load_subj():
    cmd = """
    mkdir tmp && 
    wget -O tmp/rotten_imdb.tar.gz http://www.cs.cornell.edu/people/pabo/movie-review-data/rotten_imdb.tar.gz &&
    tar -xf tmp/rotten_imdb.tar.gz -C tmp
    """
    os.system(cmd)
    X_train = []
    y_train = []
    with open('tmp/quote.tok.gt9.5000', 'r', errors='replace') as in_f:
        for el in in_f:
            X_train.append(el)
            y_train.append(1)
    with open('tmp/plot.tok.gt9.5000', 'r', errors='replace') as in_f:
        for el in in_f:
            X_train.append(el)
            y_train.append(0)

    X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.1, random_state=42)

    split_and_save(X_train, y_train, 'subj', 1/9)

    save_test(X_test, y_test, 'subj')

    cmd = "rm -r tmp"
    os.system(cmd)

    return load_from_folder('data/subj')


def create_google_automl_format(name, dataset):
    traindf = pd.DataFrame(columns=['mark', 'text', 'label'])
    traindf['text'] = dataset.X_train['text']
    traindf['label'] = dataset.y_train['label']
    traindf['mark'] = 'TRAIN'

    valdf = pd.DataFrame(columns=['mark', 'text', 'label'])
    valdf['text'] = dataset.X_val['text']
    valdf['label'] = dataset.y_val['label']
    valdf['mark'] = 'VALIDATION'

    testdf = pd.DataFrame(columns=['mark', 'text', 'label'])
    testdf['text'] = dataset.X_test['text']
    testdf['label'] = dataset.y_test['label']
    testdf['mark'] = 'TEST'

    outdf = pd.concat([traindf, valdf, testdf])
    outdf['text'] = outdf['text'].apply(lambda x: x.replace('"', "'"))
    outdf.to_csv('data/{}/google_automl_{}.csv'.format(name, name), header=None, index=None, sep=',')